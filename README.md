# Setup
- [Link1](https://rnfirebase.io/docs/v5.x.x/installation/initial-setup)
- [Link2](https://rnfirebase.io/docs/v5.x.x/installation/android)
- [Link3](https://rnfirebase.io/docs/v5.x.x/messaging/android)
- [Link4](https://rnfirebase.io/docs/v5.x.x/notifications/android)

# Code
[Git](https://gitlab.com/hoanvnbka/fcmtest)

# Note
Trong Android, khi tắt app thì sẽ không thể lấy được title và body của notification. Do đó nên gửi title và body trong data payload của notification.