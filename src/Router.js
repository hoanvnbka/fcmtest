import React from 'react';
import {createAppContainer, createStackNavigator, createBottomTabNavigator} from 'react-navigation';
import ProductDetailScreen from './screens/Product/ProductDetailScreen';
import ProductScreen from './screens/Product/ProductScreen';
import AuthorScreen from './screens/Book/AuthorScreen';
import BookScreen from './screens/Book/BookScreen';
import BookDetailScreen from './screens/Book/BookDetailScreen';
import DefaultScreen from './screens/DefaultScreen';


const ProductNavigator = createStackNavigator({
    Product: {
        screen: ProductScreen
    },
    ProductDetail: {
        screen: ProductDetailScreen,
    }
}, {
    initialRouteName: 'Product'
});

const AuthorNavigator = createStackNavigator({
    Author: {
        screen: AuthorScreen
    },
    Book: {
        screen: BookScreen
    },
    BookDetail: {
        screen: BookDetailScreen
    }
}, {
    initialRouteName: 'Author'
});

const AppNavigator = createBottomTabNavigator({
    Default: {
        screen: DefaultScreen
    },
    Author: {
        screen: AuthorNavigator
    },
    Product: {
        screen: ProductNavigator
    }
}, {
    initialRouteName: 'Default'
});


const AppContainer = createAppContainer(AppNavigator);

const MainApp = () => <AppContainer />

export default MainApp;