export const authors = [
    {
        id: 1,
        name: "Author A",
        books: [
            {
                id: 'a1',
                name: "Book A1",
                content: "Book detail A1"
            },
            {
                id: 'a2',
                name: "Book A2",
                content: "Book detail A2"
            },
            {
                id: 'a3',
                name: "Book A3",
                content: "Book detail A3"
            },
        ]
    },
    {
        id: 2,
        name: "Author B",
        books: [
            {
                id: 'b1',
                name: "Book B1",
                content: "Book detail B1"
            },
            {
                id: 'b2',
                name: "Book B2",
                content: "Book detail B2"
            }
        ]
    },
    {
        id: 3,
        name: "Author C",
        books: [
            {
                id: 'c1',
                name: "Book C1",
                content: "Book detail C1"
            },
            {
                id: 'c2',
                name: "Book C2",
                content: "Book detail C2"
            }
        ]
    }
];