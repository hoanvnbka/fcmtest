import React, { Component } from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";
import { authors } from "../../data/authors";

export default class AuthorScreen extends Component {
  static navigationOptions = {
    title: "Author"
  };

  render() {
    return (
      <View
        style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
      >
        <FlatList
          data={authors}
          renderItem={({ item }) => {
            return (
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("Book", {
                    books: item.books
                  })
                }
              >
                <Text>{item.name}</Text>
              </TouchableOpacity>
            );
          }}
          keyExtractor={item => item.id.toString()}
        />
      </View>
    );
  }
}
