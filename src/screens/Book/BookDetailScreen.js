import React, { Component } from "react";
import { Text, View } from "react-native";

export default class BookDetailScreen extends Component {
  static navigationOptions = {
    title: "Book Detail"
  };

  render() {
    const { navigation } = this.props;
    const content = navigation.getParam("content", "");

    return (
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Text>{content}</Text>
      </View>
    );
  }
}
