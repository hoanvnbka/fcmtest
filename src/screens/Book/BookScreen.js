import React, { Component } from "react";
import { Text, View, TouchableOpacity, FlatList } from "react-native";

export default class BookScreen extends Component {
  static navigationOptions = {
    title: "Book"
  };

  render() {
    const { navigation } = this.props;
    const books = navigation.getParam("books", "");

    return (
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <FlatList
          data={books}
          renderItem={({ item }) => {
            return (
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("BookDetail", {
                    content: item.content
                  })
                }
              >
                <Text>{item.name}</Text>
              </TouchableOpacity>
            );
          }}
          keyExtractor={item => item.id}
        />
      </View>
    );
  }
}
