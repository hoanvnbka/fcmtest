import React, { Component } from "react";
import { Text, View } from "react-native";
import firebase from "react-native-firebase";
import { authors } from "../data/authors";

export default class DefaultScreen extends Component {
  componentDidMount() {
    firebase
      .links()
      .getInitialLink()
      .then(url => {
        if (url) {
          this.navigate(url);
        } else {
          console.log("URL null");
        }
      })
      .catch(error => console.error(error));
    firebase.links().onLink(url => {
      if (url) {
          console.log(url);
        this.navigate(url);
      } else {
        console.log("URL null");
      }
    });
  }

  navigate = url => {
    const { navigate } = this.props.navigation;
    const route = url.replace(/.*?:\/\//g, "");
    const routeName = route.split("/")[0];
    const id = route.split("/")[1];
    const bookId = route.split("/")[2] !== null ? route.split("/")[2] : null;
    console.log(`routeName: ${routeName}`);
    console.log(`id: ${id}`);
    console.log(`authorId: ${bookId}`);

    if (routeName === "ProductDetail") {
      navigate(routeName, {
        id: id
      });
    } else if (routeName === "BookDetail") {
      for (author of authors) {
        if (author.id === parseInt(id)) {
          for (book of author.books) {
            if (book.id === bookId.toString()) {
              navigate(routeName, {
                content: book.content
              });
            }
          }
        }
      }
    }
  };

  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text style={{ fontSize: 30, fontWeight: "bold" }}>
          Welcome to Default
        </Text>
      </View>
    );
  }
}
