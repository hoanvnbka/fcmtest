import React, {Component} from 'react';
import {Text, View} from 'react-native';


export default class ProductDetailScreen extends Component {
    static navigationOptions = {
        title: 'Product Detail'
    };

    render() {
        const {navigation} = this.props;
        const id = navigation.getParam('id', '');

        return (
            <View style={{flex: 1, alignContent: 'center', alignItems: 'center'}}>
                <Text style={{fontSize: 30, fontWeight: 'bold'}}>Product detail {id}</Text>
            </View>
        );
    }
}
