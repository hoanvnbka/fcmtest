import React, {Component} from 'react';
import {Text, View, Button} from 'react-native';


export default class ProductScreen extends Component {
    static navigationOptions = {
        title: 'Product'
    };

    render() {
        return (
            <View style={{flex: 1, alignContent: 'center', alignItems: 'center'}}>
                <Text style={{fontSize: 30, fontWeight: 'bold'}}>Products</Text>
                <Button
                    title="Product Detail"
                    onPress={() => this.props.navigation.navigate('ProductDetail')}
                />
            </View>
        );
    }
}